package project5;

public class JamesBond {
	public boolean bondRegex(String s) {
		return s.replaceAll("\s", "")
				.matches("[0-9)]*[(][0-9()]*007[0-9(]*[)][()0-9]*");
	}
}
