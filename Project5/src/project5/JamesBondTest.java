package project5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class JamesBondTest {

	static JamesBond j;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		j = new JamesBond();
	}


	@Test
	public void testCase1(){
		assertFalse(j.bondRegex("( ("));
	}

	@Test
	public void testCase2(){
		assertFalse(j.bondRegex("( )"));
	}

	@Test
	public void testCase3(){
		assertTrue(j.bondRegex("( 0 0 7 )"));
	}

	@Test
	public void testCase4(){
		assertFalse(j.bondRegex("( 0 7 )"));
	}

	@Test
	public void testCase5(){
		assertFalse(j.bondRegex("( 7 )"));
	}

	@Test
	public void testCase6(){
		assertFalse(j.bondRegex("( ( ("));
	}

	@Test
	public void testCase7(){
		assertFalse(j.bondRegex("( ( )"));
	}

	@Test
	public void testCase8(){
		assertTrue(j.bondRegex("( ( 0 0 7 )"));
	}

	@Test
	public void testCase9(){
		assertFalse(j.bondRegex("( ( 0 7 )"));
	}

	@Test
	public void testCase10(){
		assertFalse(j.bondRegex("( ( 7 )"));
	}

	@Test
	public void testCase11(){
		assertFalse(j.bondRegex("( ) ("));
	}

	@Test
	public void testCase12(){
		assertFalse(j.bondRegex("( ) )"));
	}

	@Test
	public void testCase13(){
		assertTrue(j.bondRegex("( ) 0 0 7 )"));
	}

	@Test
	public void testCase14(){
		assertFalse(j.bondRegex("( ) 0 7 )"));
	}

	@Test
	public void testCase15(){
		assertFalse(j.bondRegex("( ) 7 )"));
	}

	@Test
	public void testCase16(){
		assertFalse(j.bondRegex("( 0 ("));
	}

	@Test
	public void testCase17(){
		assertFalse(j.bondRegex("( 0 )"));
	}

	@Test
	public void testCase18(){
		assertTrue(j.bondRegex("( 0 0 0 7 )"));
	}

	@Test
	public void testCase19(){
		assertFalse(j.bondRegex("( 0 ( ("));
	}

	@Test
	public void testCase20(){
		assertFalse(j.bondRegex("( 0 ( )"));
	}

	@Test
	public void testCase21(){
		assertTrue(j.bondRegex("( 0 ( 0 0 7 )"));
	}

	@Test
	public void testCase22(){
		assertFalse(j.bondRegex("( 0 ( 0 7 )"));
	}

	@Test
	public void testCase23(){
		assertFalse(j.bondRegex("( 0 ( 7 )"));
	}

	@Test
	public void testCase24(){
		assertFalse(j.bondRegex("( 0 ) ("));
	}

	@Test
	public void testCase25(){
		assertFalse(j.bondRegex("( 0 ) )"));
	}

	@Test
	public void testCase26(){
		assertTrue(j.bondRegex("( 0 ) 0 0 7 )"));
	}

	@Test
	public void testCase27(){
		assertFalse(j.bondRegex("( 0 ) 0 7 )"));
	}

	@Test
	public void testCase28(){
		assertFalse(j.bondRegex("( 0 ) 7 )"));
	}

	@Test
	public void testCase29(){
		assertFalse(j.bondRegex("( 0 0 ("));
	}

	@Test
	public void testCase30(){
		assertFalse(j.bondRegex("( 0 0 )"));
	}

	@Test
	public void testCase31(){
		assertTrue(j.bondRegex("( 0 0 0 0 7 )"));
	}

	@Test
	public void testCase32(){
		assertFalse(j.bondRegex("( 0 0 ( ("));
	}

	@Test
	public void testCase33(){
		assertFalse(j.bondRegex("( 0 0 ( )"));
	}

	@Test
	public void testCase34(){
		assertTrue(j.bondRegex("( 0 0 ( 0 0 7 )"));
	}

	@Test
	public void testCase35(){
		assertFalse(j.bondRegex("( 0 0 ( 0 7 )"));
	}

	@Test
	public void testCase36(){
		assertFalse(j.bondRegex("( 0 0 ( 7 )"));
	}

	@Test
	public void testCase37(){
		assertFalse(j.bondRegex("( 0 0 ) ("));
	}

	@Test
	public void testCase38(){
		assertFalse(j.bondRegex("( 0 0 ) )"));
	}

	@Test
	public void testCase39(){
		assertTrue(j.bondRegex("( 0 0 ) 0 0 7 )"));
	}

	@Test
	public void testCase40(){
		assertFalse(j.bondRegex("( 0 0 ) 0 7 )"));
	}

	@Test
	public void testCase41(){
		assertFalse(j.bondRegex("( 0 0 ) 7 )"));
	}

	@Test
	public void testCase42(){
		assertFalse(j.bondRegex("( 0 0 0 ("));
	}

	@Test
	public void testCase43(){
		assertFalse(j.bondRegex("( 0 0 0 )"));
	}

	@Test
	public void testCase44(){
		assertTrue(j.bondRegex("( 0 0 0 0 0 7 )"));
	}

	@Test
	public void testCase45(){
		assertFalse(j.bondRegex("( 0 0 1 ("));
	}

	@Test
	public void testCase46(){
		assertFalse(j.bondRegex("( 0 0 1 )"));
	}

	@Test
	public void testCase47(){
		assertTrue(j.bondRegex("( 0 0 1 0 0 7 )"));
	}

	@Test
	public void testCase48(){
		assertFalse(j.bondRegex("( 0 0 1 0 7 )"));
	}

	@Test
	public void testCase49(){
		assertFalse(j.bondRegex("( 0 0 1 7 )"));
	}

	@Test
	public void testCase50(){
		assertFalse(j.bondRegex("( 0 0 2 ("));
	}

	@Test
	public void testCase51(){
		assertFalse(j.bondRegex("( 0 0 2 )"));
	}

	@Test
	public void testCase52(){
		assertTrue(j.bondRegex("( 0 0 2 0 0 7 )"));
	}

	@Test
	public void testCase53(){
		assertFalse(j.bondRegex("( 0 0 2 0 7 )"));
	}

	@Test
	public void testCase54(){
		assertFalse(j.bondRegex("( 0 0 2 7 )"));
	}

	@Test
	public void testCase55(){
		assertFalse(j.bondRegex("( 0 0 3 ("));
	}

	@Test
	public void testCase56(){
		assertFalse(j.bondRegex("( 0 0 3 )"));
	}

	@Test
	public void testCase57(){
		assertTrue(j.bondRegex("( 0 0 3 0 0 7 )"));
	}

	@Test
	public void testCase58(){
		assertFalse(j.bondRegex("( 0 0 3 0 7 )"));
	}

	@Test
	public void testCase59(){
		assertFalse(j.bondRegex("( 0 0 3 7 )"));
	}

	@Test
	public void testCase60(){
		assertFalse(j.bondRegex("( 0 0 4 ("));
	}

	@Test
	public void testCase61(){
		assertFalse(j.bondRegex("( 0 0 4 )"));
	}

	@Test
	public void testCase62(){
		assertTrue(j.bondRegex("( 0 0 4 0 0 7 )"));
	}

	@Test
	public void testCase63(){
		assertFalse(j.bondRegex("( 0 0 4 0 7 )"));
	}

	@Test
	public void testCase64(){
		assertFalse(j.bondRegex("( 0 0 4 7 )"));
	}

	@Test
	public void testCase65(){
		assertFalse(j.bondRegex("( 0 0 5 ("));
	}

	@Test
	public void testCase66(){
		assertFalse(j.bondRegex("( 0 0 5 )"));
	}

	@Test
	public void testCase67(){
		assertTrue(j.bondRegex("( 0 0 5 0 0 7 )"));
	}

	@Test
	public void testCase68(){
		assertFalse(j.bondRegex("( 0 0 5 0 7 )"));
	}

	@Test
	public void testCase69(){
		assertFalse(j.bondRegex("( 0 0 5 7 )"));
	}

	@Test
	public void testCase70(){
		assertFalse(j.bondRegex("( 0 0 6 ("));
	}

	@Test
	public void testCase71(){
		assertFalse(j.bondRegex("( 0 0 6 )"));
	}

	@Test
	public void testCase72(){
		assertTrue(j.bondRegex("( 0 0 6 0 0 7 )"));
	}

	@Test
	public void testCase73(){
		assertFalse(j.bondRegex("( 0 0 6 0 7 )"));
	}

	@Test
	public void testCase74(){
		assertFalse(j.bondRegex("( 0 0 6 7 )"));
	}

	@Test
	public void testCase75(){
		assertFalse(j.bondRegex("( 0 0 7 ("));
	}

	@Test
	public void testCase76(){
		assertTrue(j.bondRegex("( 0 0 7 0 0 7 )"));
	}

	@Test
	public void testCase77(){
		assertTrue(j.bondRegex("( 0 0 7 0 7 )"));
	}

	@Test
	public void testCase78(){
		assertTrue(j.bondRegex("( 0 0 7 7 )"));
	}

	@Test
	public void testCase79(){
		assertFalse(j.bondRegex("( 0 0 7 ( ("));
	}

	@Test
	public void testCase80(){
		assertTrue(j.bondRegex("( 0 0 7 ( )"));
	}

	@Test
	public void testCase81(){
		assertTrue(j.bondRegex("( 0 0 7 ( 0 0 7 )"));
	}

	@Test
	public void testCase82(){
		assertTrue(j.bondRegex("( 0 0 7 ( 0 7 )"));
	}

	@Test
	public void testCase83(){
		assertTrue(j.bondRegex("( 0 0 7 ( 7 )"));
	}

	@Test
	public void testCase84(){
		assertTrue(j.bondRegex("( 0 0 7 ) ("));
	}

	@Test
	public void testCase85(){
		assertTrue(j.bondRegex("( 0 0 7 ) )"));
	}

	@Test
	public void testCase86(){
		assertTrue(j.bondRegex("( 0 0 7 ) 0 0 7 )"));
	}

	@Test
	public void testCase87(){
		assertTrue(j.bondRegex("( 0 0 7 ) 0 7 )"));
	}

	@Test
	public void testCase88(){
		assertTrue(j.bondRegex("( 0 0 7 ) 7 )"));
	}

	@Test
	public void testCase89(){
		assertTrue(j.bondRegex("( 0 0 7 ) ( ("));
	}

	@Test
	public void testCase90(){
		assertTrue(j.bondRegex("( 0 0 7 ) ( )"));
	}

	@Test
	public void testCase91(){
		assertTrue(j.bondRegex("( 0 0 7 ) ( 0 0 7 )"));
	}

	@Test
	public void testCase92(){
		assertTrue(j.bondRegex("( 0 0 7 ) ( 0 7 )"));
	}

	@Test
	public void testCase93(){
		assertTrue(j.bondRegex("( 0 0 7 ) ( 7 )"));
	}

	@Test
	public void testCase94(){
		assertTrue(j.bondRegex("( 0 0 7 ) ) ("));
	}

	@Test
	public void testCase95(){
		assertTrue(j.bondRegex("( 0 0 7 ) ) )"));
	}

	@Test
	public void testCase96(){
		assertTrue(j.bondRegex("( 0 0 7 ) ) 0 0 7 )"));
	}

	@Test
	public void testCase97(){
		assertTrue(j.bondRegex("( 0 0 7 ) ) 0 7 )"));
	}

	@Test
	public void testCase98(){
		assertTrue(j.bondRegex("( 0 0 7 ) ) 7 )"));
	}

	@Test
	public void testCase99(){
		assertTrue(j.bondRegex("( 0 0 7 ) 0 ("));
	}

	@Test
	public void testCase100(){
		assertTrue(j.bondRegex("( 0 0 7 ) 0 )"));
	}

	@Test
	public void testCase101(){
		assertTrue(j.bondRegex("( 0 0 7 ) 0 0 0 7 )"));
	}

	@Test
	public void testCase102(){
		assertTrue(j.bondRegex("( 0 0 7 ) 1 ("));
	}

	@Test
	public void testCase103(){
		assertTrue(j.bondRegex("( 0 0 7 ) 1 )"));
	}

	@Test
	public void testCase104(){
		assertTrue(j.bondRegex("( 0 0 7 ) 1 0 0 7 )"));
	}

	@Test
	public void testCase105(){
		assertTrue(j.bondRegex("( 0 0 7 ) 1 0 7 )"));
	}

	@Test
	public void testCase106(){
		assertTrue(j.bondRegex("( 0 0 7 ) 1 7 )"));
	}

	@Test
	public void testCase107(){
		assertTrue(j.bondRegex("( 0 0 7 ) 2 ("));
	}

	@Test
	public void testCase108(){
		assertTrue(j.bondRegex("( 0 0 7 ) 2 )"));
	}

	@Test
	public void testCase109(){
		assertTrue(j.bondRegex("( 0 0 7 ) 2 0 0 7 )"));
	}

	@Test
	public void testCase110(){
		assertTrue(j.bondRegex("( 0 0 7 ) 2 0 7 )"));
	}

	@Test
	public void testCase111(){
		assertTrue(j.bondRegex("( 0 0 7 ) 2 7 )"));
	}

	@Test
	public void testCase112(){
		assertTrue(j.bondRegex("( 0 0 7 ) 3 ("));
	}

	@Test
	public void testCase113(){
		assertTrue(j.bondRegex("( 0 0 7 ) 3 )"));
	}

	@Test
	public void testCase114(){
		assertTrue(j.bondRegex("( 0 0 7 ) 3 0 0 7 )"));
	}

	@Test
	public void testCase115(){
		assertTrue(j.bondRegex("( 0 0 7 ) 3 0 7 )"));
	}

	@Test
	public void testCase116(){
		assertTrue(j.bondRegex("( 0 0 7 ) 3 7 )"));
	}

	@Test
	public void testCase117(){
		assertTrue(j.bondRegex("( 0 0 7 ) 4 ("));
	}

	@Test
	public void testCase118(){
		assertTrue(j.bondRegex("( 0 0 7 ) 4 )"));
	}

	@Test
	public void testCase119(){
		assertTrue(j.bondRegex("( 0 0 7 ) 4 0 0 7 )"));
	}

	@Test
	public void testCase120(){
		assertTrue(j.bondRegex("( 0 0 7 ) 4 0 7 )"));
	}

	@Test
	public void testCase121(){
		assertTrue(j.bondRegex("( 0 0 7 ) 4 7 )"));
	}

	@Test
	public void testCase122(){
		assertTrue(j.bondRegex("( 0 0 7 ) 5 ("));
	}

	@Test
	public void testCase123(){
		assertTrue(j.bondRegex("( 0 0 7 ) 5 )"));
	}

	@Test
	public void testCase124(){
		assertTrue(j.bondRegex("( 0 0 7 ) 5 0 0 7 )"));
	}

	@Test
	public void testCase125(){
		assertTrue(j.bondRegex("( 0 0 7 ) 5 0 7 )"));
	}

	@Test
	public void testCase126(){
		assertTrue(j.bondRegex("( 0 0 7 ) 5 7 )"));
	}

	@Test
	public void testCase127(){
		assertTrue(j.bondRegex("( 0 0 7 ) 6 ("));
	}

	@Test
	public void testCase128(){
		assertTrue(j.bondRegex("( 0 0 7 ) 6 )"));
	}

	@Test
	public void testCase129(){
		assertTrue(j.bondRegex("( 0 0 7 ) 6 0 0 7 )"));
	}

	@Test
	public void testCase130(){
		assertTrue(j.bondRegex("( 0 0 7 ) 6 0 7 )"));
	}

	@Test
	public void testCase131(){
		assertTrue(j.bondRegex("( 0 0 7 ) 6 7 )"));
	}

	@Test
	public void testCase132(){
		assertTrue(j.bondRegex("( 0 0 7 ) 7 ("));
	}

	@Test
	public void testCase133(){
		assertTrue(j.bondRegex("( 0 0 7 ) 7 0 0 7 )"));
	}

	@Test
	public void testCase134(){
		assertTrue(j.bondRegex("( 0 0 7 ) 7 0 7 )"));
	}

	@Test
	public void testCase135(){
		assertTrue(j.bondRegex("( 0 0 7 ) 7 7 )"));
	}

	@Test
	public void testCase136(){
		assertTrue(j.bondRegex("( 0 0 7 ) 8 ("));
	}

	@Test
	public void testCase137(){
		assertTrue(j.bondRegex("( 0 0 7 ) 8 )"));
	}

	@Test
	public void testCase138(){
		assertTrue(j.bondRegex("( 0 0 7 ) 8 0 0 7 )"));
	}

	@Test
	public void testCase139(){
		assertTrue(j.bondRegex("( 0 0 7 ) 8 0 7 )"));
	}

	@Test
	public void testCase140(){
		assertTrue(j.bondRegex("( 0 0 7 ) 8 7 )"));
	}

	@Test
	public void testCase141(){
		assertTrue(j.bondRegex("( 0 0 7 ) 9 ("));
	}

	@Test
	public void testCase142(){
		assertTrue(j.bondRegex("( 0 0 7 ) 9 )"));
	}

	@Test
	public void testCase143(){
		assertTrue(j.bondRegex("( 0 0 7 ) 9 0 0 7 )"));
	}

	@Test
	public void testCase144(){
		assertTrue(j.bondRegex("( 0 0 7 ) 9 0 7 )"));
	}

	@Test
	public void testCase145(){
		assertTrue(j.bondRegex("( 0 0 7 ) 9 7 )"));
	}

	@Test
	public void testCase146(){
		assertFalse(j.bondRegex("( 0 0 7 0 ("));
	}

	@Test
	public void testCase147(){
		assertTrue(j.bondRegex("( 0 0 7 0 )"));
	}

	@Test
	public void testCase148(){
		assertTrue(j.bondRegex("( 0 0 7 0 0 0 7 )"));
	}

	@Test
	public void testCase149(){
		assertFalse(j.bondRegex("( 0 0 7 1 ("));
	}

	@Test
	public void testCase150(){
		assertTrue(j.bondRegex("( 0 0 7 1 )"));
	}

	@Test
	public void testCase151(){
		assertTrue(j.bondRegex("( 0 0 7 1 0 0 7 )"));
	}

	@Test
	public void testCase152(){
		assertTrue(j.bondRegex("( 0 0 7 1 0 7 )"));
	}

	@Test
	public void testCase153(){
		assertTrue(j.bondRegex("( 0 0 7 1 7 )"));
	}

	@Test
	public void testCase154(){
		assertFalse(j.bondRegex("( 0 0 7 2 ("));
	}

	@Test
	public void testCase155(){
		assertTrue(j.bondRegex("( 0 0 7 2 )"));
	}

	@Test
	public void testCase156(){
		assertTrue(j.bondRegex("( 0 0 7 2 0 0 7 )"));
	}

	@Test
	public void testCase157(){
		assertTrue(j.bondRegex("( 0 0 7 2 0 7 )"));
	}

	@Test
	public void testCase158(){
		assertTrue(j.bondRegex("( 0 0 7 2 7 )"));
	}

	@Test
	public void testCase159(){
		assertFalse(j.bondRegex("( 0 0 7 3 ("));
	}

	@Test
	public void testCase160(){
		assertTrue(j.bondRegex("( 0 0 7 3 )"));
	}

	@Test
	public void testCase161(){
		assertTrue(j.bondRegex("( 0 0 7 3 0 0 7 )"));
	}

	@Test
	public void testCase162(){
		assertTrue(j.bondRegex("( 0 0 7 3 0 7 )"));
	}

	@Test
	public void testCase163(){
		assertTrue(j.bondRegex("( 0 0 7 3 7 )"));
	}

	@Test
	public void testCase164(){
		assertFalse(j.bondRegex("( 0 0 7 4 ("));
	}

	@Test
	public void testCase165(){
		assertTrue(j.bondRegex("( 0 0 7 4 )"));
	}

	@Test
	public void testCase166(){
		assertTrue(j.bondRegex("( 0 0 7 4 0 0 7 )"));
	}

	@Test
	public void testCase167(){
		assertTrue(j.bondRegex("( 0 0 7 4 0 7 )"));
	}

	@Test
	public void testCase168(){
		assertTrue(j.bondRegex("( 0 0 7 4 7 )"));
	}

	@Test
	public void testCase169(){
		assertFalse(j.bondRegex("( 0 0 7 5 ("));
	}

	@Test
	public void testCase170(){
		assertTrue(j.bondRegex("( 0 0 7 5 )"));
	}

	@Test
	public void testCase171(){
		assertTrue(j.bondRegex("( 0 0 7 5 0 0 7 )"));
	}

	@Test
	public void testCase172(){
		assertTrue(j.bondRegex("( 0 0 7 5 0 7 )"));
	}

	@Test
	public void testCase173(){
		assertTrue(j.bondRegex("( 0 0 7 5 7 )"));
	}

	@Test
	public void testCase174(){
		assertFalse(j.bondRegex("( 0 0 7 6 ("));
	}

	@Test
	public void testCase175(){
		assertTrue(j.bondRegex("( 0 0 7 6 )"));
	}

	@Test
	public void testCase176(){
		assertTrue(j.bondRegex("( 0 0 7 6 0 0 7 )"));
	}

	@Test
	public void testCase177(){
		assertTrue(j.bondRegex("( 0 0 7 6 0 7 )"));
	}

	@Test
	public void testCase178(){
		assertTrue(j.bondRegex("( 0 0 7 6 7 )"));
	}

	@Test
	public void testCase179(){
		assertFalse(j.bondRegex("( 0 0 7 7 ("));
	}

	@Test
	public void testCase180(){
		assertTrue(j.bondRegex("( 0 0 7 7 0 0 7 )"));
	}

	@Test
	public void testCase181(){
		assertTrue(j.bondRegex("( 0 0 7 7 0 7 )"));
	}

	@Test
	public void testCase182(){
		assertTrue(j.bondRegex("( 0 0 7 7 7 )"));
	}

	@Test
	public void testCase183(){
		assertFalse(j.bondRegex("( 0 0 7 8 ("));
	}

	@Test
	public void testCase184(){
		assertTrue(j.bondRegex("( 0 0 7 8 )"));
	}

	@Test
	public void testCase185(){
		assertTrue(j.bondRegex("( 0 0 7 8 0 0 7 )"));
	}

	@Test
	public void testCase186(){
		assertTrue(j.bondRegex("( 0 0 7 8 0 7 )"));
	}

	@Test
	public void testCase187(){
		assertTrue(j.bondRegex("( 0 0 7 8 7 )"));
	}

	@Test
	public void testCase188(){
		assertFalse(j.bondRegex("( 0 0 7 9 ("));
	}

	@Test
	public void testCase189(){
		assertTrue(j.bondRegex("( 0 0 7 9 )"));
	}

	@Test
	public void testCase190(){
		assertTrue(j.bondRegex("( 0 0 7 9 0 0 7 )"));
	}

	@Test
	public void testCase191(){
		assertTrue(j.bondRegex("( 0 0 7 9 0 7 )"));
	}

	@Test
	public void testCase192(){
		assertTrue(j.bondRegex("( 0 0 7 9 7 )"));
	}

	@Test
	public void testCase193(){
		assertFalse(j.bondRegex("( 0 0 8 ("));
	}

	@Test
	public void testCase194(){
		assertFalse(j.bondRegex("( 0 0 8 )"));
	}

	@Test
	public void testCase195(){
		assertTrue(j.bondRegex("( 0 0 8 0 0 7 )"));
	}

	@Test
	public void testCase196(){
		assertFalse(j.bondRegex("( 0 0 8 0 7 )"));
	}

	@Test
	public void testCase197(){
		assertFalse(j.bondRegex("( 0 0 8 7 )"));
	}

	@Test
	public void testCase198(){
		assertFalse(j.bondRegex("( 0 0 9 ("));
	}

	@Test
	public void testCase199(){
		assertFalse(j.bondRegex("( 0 0 9 )"));
	}

	@Test
	public void testCase200(){
		assertTrue(j.bondRegex("( 0 0 9 0 0 7 )"));
	}

	@Test
	public void testCase201(){
		assertFalse(j.bondRegex("( 0 0 9 0 7 )"));
	}

	@Test
	public void testCase202(){
		assertFalse(j.bondRegex("( 0 0 9 7 )"));
	}

	@Test
	public void testCase203(){
		assertFalse(j.bondRegex("( 0 1 ("));
	}

	@Test
	public void testCase204(){
		assertFalse(j.bondRegex("( 0 1 )"));
	}

	@Test
	public void testCase205(){
		assertTrue(j.bondRegex("( 0 1 0 0 7 )"));
	}

	@Test
	public void testCase206(){
		assertFalse(j.bondRegex("( 0 1 0 7 )"));
	}

	@Test
	public void testCase207(){
		assertFalse(j.bondRegex("( 0 1 7 )"));
	}

	@Test
	public void testCase208(){
		assertFalse(j.bondRegex("( 0 2 ("));
	}

	@Test
	public void testCase209(){
		assertFalse(j.bondRegex("( 0 2 )"));
	}

	@Test
	public void testCase210(){
		assertTrue(j.bondRegex("( 0 2 0 0 7 )"));
	}

	@Test
	public void testCase211(){
		assertFalse(j.bondRegex("( 0 2 0 7 )"));
	}

	@Test
	public void testCase212(){
		assertFalse(j.bondRegex("( 0 2 7 )"));
	}

	@Test
	public void testCase213(){
		assertFalse(j.bondRegex("( 0 3 ("));
	}

	@Test
	public void testCase214(){
		assertFalse(j.bondRegex("( 0 3 )"));
	}

	@Test
	public void testCase215(){
		assertTrue(j.bondRegex("( 0 3 0 0 7 )"));
	}

	@Test
	public void testCase216(){
		assertFalse(j.bondRegex("( 0 3 0 7 )"));
	}

	@Test
	public void testCase217(){
		assertFalse(j.bondRegex("( 0 3 7 )"));
	}

	@Test
	public void testCase218(){
		assertFalse(j.bondRegex("( 0 4 ("));
	}

	@Test
	public void testCase219(){
		assertFalse(j.bondRegex("( 0 4 )"));
	}

	@Test
	public void testCase220(){
		assertTrue(j.bondRegex("( 0 4 0 0 7 )"));
	}

	@Test
	public void testCase221(){
		assertFalse(j.bondRegex("( 0 4 0 7 )"));
	}

	@Test
	public void testCase222(){
		assertFalse(j.bondRegex("( 0 4 7 )"));
	}

	@Test
	public void testCase223(){
		assertFalse(j.bondRegex("( 0 5 ("));
	}

	@Test
	public void testCase224(){
		assertFalse(j.bondRegex("( 0 5 )"));
	}

	@Test
	public void testCase225(){
		assertTrue(j.bondRegex("( 0 5 0 0 7 )"));
	}

	@Test
	public void testCase226(){
		assertFalse(j.bondRegex("( 0 5 0 7 )"));
	}

	@Test
	public void testCase227(){
		assertFalse(j.bondRegex("( 0 5 7 )"));
	}

	@Test
	public void testCase228(){
		assertFalse(j.bondRegex("( 0 6 ("));
	}

	@Test
	public void testCase229(){
		assertFalse(j.bondRegex("( 0 6 )"));
	}

	@Test
	public void testCase230(){
		assertTrue(j.bondRegex("( 0 6 0 0 7 )"));
	}

	@Test
	public void testCase231(){
		assertFalse(j.bondRegex("( 0 6 0 7 )"));
	}

	@Test
	public void testCase232(){
		assertFalse(j.bondRegex("( 0 6 7 )"));
	}

	@Test
	public void testCase233(){
		assertFalse(j.bondRegex("( 0 7 ("));
	}

	@Test
	public void testCase234(){
		assertTrue(j.bondRegex("( 0 7 0 0 7 )"));
	}

	@Test
	public void testCase235(){
		assertFalse(j.bondRegex("( 0 7 0 7 )"));
	}

	@Test
	public void testCase236(){
		assertFalse(j.bondRegex("( 0 7 7 )"));
	}

	@Test
	public void testCase237(){
		assertFalse(j.bondRegex("( 0 8 ("));
	}

	@Test
	public void testCase238(){
		assertFalse(j.bondRegex("( 0 8 )"));
	}

	@Test
	public void testCase239(){
		assertTrue(j.bondRegex("( 0 8 0 0 7 )"));
	}

	@Test
	public void testCase240(){
		assertFalse(j.bondRegex("( 0 8 0 7 )"));
	}

	@Test
	public void testCase241(){
		assertFalse(j.bondRegex("( 0 8 7 )"));
	}

	@Test
	public void testCase242(){
		assertFalse(j.bondRegex("( 0 9 ("));
	}

	@Test
	public void testCase243(){
		assertFalse(j.bondRegex("( 0 9 )"));
	}

	@Test
	public void testCase244(){
		assertTrue(j.bondRegex("( 0 9 0 0 7 )"));
	}

	@Test
	public void testCase245(){
		assertFalse(j.bondRegex("( 0 9 0 7 )"));
	}

	@Test
	public void testCase246(){
		assertFalse(j.bondRegex("( 0 9 7 )"));
	}

	@Test
	public void testCase247(){
		assertFalse(j.bondRegex("( 1 ("));
	}

	@Test
	public void testCase248(){
		assertFalse(j.bondRegex("( 1 )"));
	}

	@Test
	public void testCase249(){
		assertTrue(j.bondRegex("( 1 0 0 7 )"));
	}

	@Test
	public void testCase250(){
		assertFalse(j.bondRegex("( 1 0 7 )"));
	}

	@Test
	public void testCase251(){
		assertFalse(j.bondRegex("( 1 7 )"));
	}

	@Test
	public void testCase252(){
		assertFalse(j.bondRegex("( 2 ("));
	}

	@Test
	public void testCase253(){
		assertFalse(j.bondRegex("( 2 )"));
	}

	@Test
	public void testCase254(){
		assertTrue(j.bondRegex("( 2 0 0 7 )"));
	}

	@Test
	public void testCase255(){
		assertFalse(j.bondRegex("( 2 0 7 )"));
	}

	@Test
	public void testCase256(){
		assertFalse(j.bondRegex("( 2 7 )"));
	}

	@Test
	public void testCase257(){
		assertFalse(j.bondRegex("( 3 ("));
	}

	@Test
	public void testCase258(){
		assertFalse(j.bondRegex("( 3 )"));
	}

	@Test
	public void testCase259(){
		assertTrue(j.bondRegex("( 3 0 0 7 )"));
	}

	@Test
	public void testCase260(){
		assertFalse(j.bondRegex("( 3 0 7 )"));
	}

	@Test
	public void testCase261(){
		assertFalse(j.bondRegex("( 3 7 )"));
	}

	@Test
	public void testCase262(){
		assertFalse(j.bondRegex("( 4 ("));
	}

	@Test
	public void testCase263(){
		assertFalse(j.bondRegex("( 4 )"));
	}

	@Test
	public void testCase264(){
		assertTrue(j.bondRegex("( 4 0 0 7 )"));
	}

	@Test
	public void testCase265(){
		assertFalse(j.bondRegex("( 4 0 7 )"));
	}

	@Test
	public void testCase266(){
		assertFalse(j.bondRegex("( 4 7 )"));
	}

	@Test
	public void testCase267(){
		assertFalse(j.bondRegex("( 5 ("));
	}

	@Test
	public void testCase268(){
		assertFalse(j.bondRegex("( 5 )"));
	}

	@Test
	public void testCase269(){
		assertTrue(j.bondRegex("( 5 0 0 7 )"));
	}

	@Test
	public void testCase270(){
		assertFalse(j.bondRegex("( 5 0 7 )"));
	}

	@Test
	public void testCase271(){
		assertFalse(j.bondRegex("( 5 7 )"));
	}

	@Test
	public void testCase272(){
		assertFalse(j.bondRegex("( 6 ("));
	}

	@Test
	public void testCase273(){
		assertFalse(j.bondRegex("( 6 )"));
	}

	@Test
	public void testCase274(){
		assertTrue(j.bondRegex("( 6 0 0 7 )"));
	}

	@Test
	public void testCase275(){
		assertFalse(j.bondRegex("( 6 0 7 )"));
	}

	@Test
	public void testCase276(){
		assertFalse(j.bondRegex("( 6 7 )"));
	}

	@Test
	public void testCase277(){
		assertFalse(j.bondRegex("( 7 ("));
	}

	@Test
	public void testCase278(){
		assertTrue(j.bondRegex("( 7 0 0 7 )"));
	}

	@Test
	public void testCase279(){
		assertFalse(j.bondRegex("( 7 0 7 )"));
	}

	@Test
	public void testCase280(){
		assertFalse(j.bondRegex("( 7 7 )"));
	}

	@Test
	public void testCase281(){
		assertFalse(j.bondRegex("( 8 ("));
	}

	@Test
	public void testCase282(){
		assertFalse(j.bondRegex("( 8 )"));
	}

	@Test
	public void testCase283(){
		assertTrue(j.bondRegex("( 8 0 0 7 )"));
	}

	@Test
	public void testCase284(){
		assertFalse(j.bondRegex("( 8 0 7 )"));
	}

	@Test
	public void testCase285(){
		assertFalse(j.bondRegex("( 8 7 )"));
	}

	@Test
	public void testCase286(){
		assertFalse(j.bondRegex("( 9 ("));
	}

	@Test
	public void testCase287(){
		assertFalse(j.bondRegex("( 9 )"));
	}

	@Test
	public void testCase288(){
		assertTrue(j.bondRegex("( 9 0 0 7 )"));
	}

	@Test
	public void testCase289(){
		assertFalse(j.bondRegex("( 9 0 7 )"));
	}

	@Test
	public void testCase290(){
		assertFalse(j.bondRegex("( 9 7 )"));
	}

	@Test
	public void testCase291(){
		assertFalse(j.bondRegex(") ("));
	}

	@Test
	public void testCase292(){
		assertFalse(j.bondRegex(") )"));
	}

	@Test
	public void testCase293(){
		assertFalse(j.bondRegex(") 0 0 7 )"));
	}

	@Test
	public void testCase294(){
		assertFalse(j.bondRegex(") 0 7 )"));
	}

	@Test
	public void testCase295(){
		assertFalse(j.bondRegex(") 7 )"));
	}

	@Test
	public void testCase296(){
		assertFalse(j.bondRegex("0 ("));
	}

	@Test
	public void testCase297(){
		assertFalse(j.bondRegex("0 )"));
	}

	@Test
	public void testCase298(){
		assertFalse(j.bondRegex("0 0 0 7 )"));
	}

	@Test
	public void testCase299(){
		assertFalse(j.bondRegex("0 0 7 )"));
	}

	@Test
	public void testCase300(){
		assertFalse(j.bondRegex("0 7 )"));
	}

	@Test
	public void testCase301(){
		assertFalse(j.bondRegex("1 ("));
	}

	@Test
	public void testCase302(){
		assertFalse(j.bondRegex("1 )"));
	}

	@Test
	public void testCase303(){
		assertFalse(j.bondRegex("1 0 0 7 )"));
	}

	@Test
	public void testCase304(){
		assertFalse(j.bondRegex("1 0 7 )"));
	}

	@Test
	public void testCase305(){
		assertFalse(j.bondRegex("1 7 )"));
	}

	@Test
	public void testCase306(){
		assertFalse(j.bondRegex("2 ("));
	}

	@Test
	public void testCase307(){
		assertFalse(j.bondRegex("2 )"));
	}

	@Test
	public void testCase308(){
		assertFalse(j.bondRegex("2 0 0 7 )"));
	}

	@Test
	public void testCase309(){
		assertFalse(j.bondRegex("2 0 7 )"));
	}

	@Test
	public void testCase310(){
		assertFalse(j.bondRegex("2 7 )"));
	}

	@Test
	public void testCase311(){
		assertFalse(j.bondRegex("3 ("));
	}

	@Test
	public void testCase312(){
		assertFalse(j.bondRegex("3 )"));
	}

	@Test
	public void testCase313(){
		assertFalse(j.bondRegex("3 0 0 7 )"));
	}

	@Test
	public void testCase314(){
		assertFalse(j.bondRegex("3 0 7 )"));
	}

	@Test
	public void testCase315(){
		assertFalse(j.bondRegex("3 7 )"));
	}

	@Test
	public void testCase316(){
		assertFalse(j.bondRegex("4 ("));
	}

	@Test
	public void testCase317(){
		assertFalse(j.bondRegex("4 )"));
	}

	@Test
	public void testCase318(){
		assertFalse(j.bondRegex("4 0 0 7 )"));
	}

	@Test
	public void testCase319(){
		assertFalse(j.bondRegex("4 0 7 )"));
	}

	@Test
	public void testCase320(){
		assertFalse(j.bondRegex("4 7 )"));
	}

	@Test
	public void testCase321(){
		assertFalse(j.bondRegex("5 ("));
	}

	@Test
	public void testCase322(){
		assertFalse(j.bondRegex("5 )"));
	}

	@Test
	public void testCase323(){
		assertFalse(j.bondRegex("5 0 0 7 )"));
	}

	@Test
	public void testCase324(){
		assertFalse(j.bondRegex("5 0 7 )"));
	}

	@Test
	public void testCase325(){
		assertFalse(j.bondRegex("5 7 )"));
	}

	@Test
	public void testCase326(){
		assertFalse(j.bondRegex("6 ("));
	}

	@Test
	public void testCase327(){
		assertFalse(j.bondRegex("6 )"));
	}

	@Test
	public void testCase328(){
		assertFalse(j.bondRegex("6 0 0 7 )"));
	}

	@Test
	public void testCase329(){
		assertFalse(j.bondRegex("6 0 7 )"));
	}

	@Test
	public void testCase330(){
		assertFalse(j.bondRegex("6 7 )"));
	}

	@Test
	public void testCase331(){
		assertFalse(j.bondRegex("7 ("));
	}

	@Test
	public void testCase332(){
		assertFalse(j.bondRegex("7 )"));
	}

	@Test
	public void testCase333(){
		assertFalse(j.bondRegex("7 0 0 7 )"));
	}

	@Test
	public void testCase334(){
		assertFalse(j.bondRegex("7 0 7 )"));
	}

	@Test
	public void testCase335(){
		assertFalse(j.bondRegex("7 7 )"));
	}

	@Test
	public void testCase336(){
		assertFalse(j.bondRegex("8 ("));
	}

	@Test
	public void testCase337(){
		assertFalse(j.bondRegex("8 )"));
	}

	@Test
	public void testCase338(){
		assertFalse(j.bondRegex("8 0 0 7 )"));
	}

	@Test
	public void testCase339(){
		assertFalse(j.bondRegex("8 0 7 )"));
	}

	@Test
	public void testCase340(){
		assertFalse(j.bondRegex("8 7 )"));
	}

	@Test
	public void testCase341(){
		assertFalse(j.bondRegex("9 ("));
	}

	@Test
	public void testCase342(){
		assertFalse(j.bondRegex("9 )"));
	}

	@Test
	public void testCase343(){
		assertFalse(j.bondRegex("9 0 0 7 )"));
	}

	@Test
	public void testCase344(){
		assertFalse(j.bondRegex("9 0 7 )"));
	}

	@Test
	public void testCase345(){
		assertFalse(j.bondRegex("9 7 )"));
	}

	@Test
	public void testCase346(){
		assertFalse(j.bondRegex("("));
	}

	@Test
	public void testCase347(){
		assertFalse(j.bondRegex(")"));
	}

	
}